classdef Simulation <handle
    
    properties
        node;
        num_simulations;
        epochs;
        num_trials;
        num_agents;
        num_opponents;
                
        eGreedy_results;
        EXP3_results;
        Arm_results;
        EXP3S_results;
    end
    
    methods
        
        function self=Simulation(varargin)
            self.num_simulations = varargin{1};
            self.epochs = varargin{2};
            self.num_trials = varargin{3};
            self.num_agents = varargin{4};
            self.num_opponents = varargin{5};
  
            self.eGreedy_results = zeros(self.epochs*self.num_trials,1);
            self.EXP3_results = zeros(self.epochs*self.num_trials,1);
            self.Arm_results = zeros(self.epochs*self.num_trials, 4); % 1st to 3rd columns correspond to each arm and the last one is the BestARM in every trial
            self.EXP3S_results = zeros(self.epochs*self.num_trials,1);
        end
        
        function createAgentsandOpponents(self, exp_factor)
            self.node = Node(1);
            k = self.num_opponents; %number of arms/actions
                
            %EXP3 - MAB code
            self.node.exp3_bandit = EXP3(k, exp_factor);

            %eGreedy - MAB code
            self.node.egreedy_bandit = eGreedy(k, exp_factor);
            
            %EXP3.S - MAB code
            self.node.exp3s_bandit = EXP3S(k, exp_factor);
        end
        
        function solution = runSimulations(self, exp_factor)
            
            solution = zeros(self.num_simulations,5); %5 variables: eGreedy, EXP3, EXP3.S, best single action, optimal action
                        
            for j=1: self.num_simulations

                self.createAgentsandOpponents(exp_factor);
                negotiator = self.node;
                negotiator.env_case = randi(3);

                for l=1: self.epochs
                    for m=1: self.num_trials
                        trial = (l-1)*self.num_trials+m;
                        
                        %Rewards and arms + optimo and its reward in every
                        %trial
                        negotiator.getArmsRewards();
                        self.Arm_results(trial,:) = negotiator.rewards_arr;

                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %eGreedy - The negotiator chooses an opponent using eGreedy
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        reward = negotiator.executeeGreedy();
                        self.eGreedy_results(trial,1) = reward;

                        %Rescaling weights, because of Inf and NaN by keeping the proportions between the arms' weights
                        if mod(trial,50)==0 %Every 50 iterations
                            negotiator.fixNaN;
                        end
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %EXP3 - The negotiator chooses an opponent using EXP3
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        reward = negotiator.executeEXP3();
                        self.EXP3_results(trial,1) = reward;
                        
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        %EXP3.S - The negotiator chooses an opponent using
                        %EXP3.S
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        reward = negotiator.executeEXP3S();
                        self.EXP3S_results(trial,1) = reward;
                        
                    end
                    negotiator.env_case = randi(3);
                end
                
                arm1 = sum(self.Arm_results(:,1));
                arm2 = sum(self.Arm_results(:,2));
                arm3 = sum(self.Arm_results(:,3));
                optimal_arm_cum_rew = sum(self.Arm_results(:,4)); %cumulative reward of optimal arm
                
                [max_value,~] = max([arm1,arm2,arm3]); %cumulative reward of best single fixed arm in this simulation of 5000 trials
                
                solution(j,1) = sum(self.eGreedy_results);
                solution(j,2) = sum(self.EXP3_results);
                solution(j,3) = sum(self.EXP3S_results);
                solution(j,4) = max_value; %best single fixed action cumulative reward in this simulation
                solution(j,5) = optimal_arm_cum_rew; %cumulative reward of best single fixed action in this simulation
                
                self.eGreedy_results = zeros(self.epochs*self.num_trials,1);
                self.EXP3_results = zeros(self.epochs*self.num_trials,1);
                self.Arm_results = zeros(self.epochs*self.num_trials,4);
                self.EXP3S_results = zeros(self.epochs*self.num_trials,1);
            end
        end    
    end
end
