## Exploration rate tuning sample

Code to tune exploration rate in simple scenario: arm selection from a set of arms.

## Requirements to deploy

None, clone the project and run the main MATLAB script.
