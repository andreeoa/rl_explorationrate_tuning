%EXP3.S is similar to EXP3, but its exploration factor depends on H

classdef EXP3S <handle
    
    properties
        n_arms; %number of actions, in this case: number of opponents
        gamma; %egalitarianism factor [0,1), defines amount of exploration, the closer to 1 the more exploration the agent does
        w; %weights
        p; %distribution
        
        %For the regret calculation
        cumulativeReward;
    end
    
    methods
        
        function self = EXP3S(varargin)
            self.n_arms = varargin{1};
            self.gamma = varargin{2}; %the higher gamma, the higher the exploration
            self.w = zeros(self.n_arms,1); %Exp3.S uses these weights to decide randomly which action to take next,
            self.w(:) = 1; %Initialisation of weights
            self.p = zeros(self.n_arms,1); %probabilities
            
            %For the regret calculation
            self.cumulativeReward = 0;
        end
        
        function setProbability_i_at_t(self)
            for i=1:self.n_arms
                %Each value p(i) is a probability for an action i
                %gamma, the closer to 1 (then the weights are less meaningful), the more exploration the agent does
                self.p(i) = (1-self.gamma)* (self.w(i)/sum(self.w)) + (self.gamma/self.n_arms); 
            end
        end
        
        %Randomization is introduced into the action selection process,
        %which is known as the mixed strategy. That is, instead of selecting a single action at each time instance t, the player chooses a probability distribution Pt = (p_1,t,..., p_K,t) over the set of K actions, and plays action i with probability p_i,t .
        %This function returns index of arms based on probabilities
        function idx =  drawOpponentAccordingProbability(self)
            normalised_weights = zeros(self.n_arms,1);
            %Normalize the weights such that sum of probabilities equal to 1
            for i=1:self.n_arms
                normalised_weights(i)=self.p(i)/sum(self.p);
            end

            %Random generator for arm index based on probabilities
            r = rand; %using uniform distribution
            idx = 1;
            interval_s = 0; % start interval
            interval_e = 0; % end interval
            for i=1:self.n_arms
                interval_e = interval_e + normalised_weights(i,1);
                if r >interval_s && r <= interval_e
                    idx = i;
                    break;
                end
                interval_s = interval_e;
            end
        end
        
        %In each step/trial/round, the player has access to the history of rewards for 
        %the actions that were chosen by the algorithm in previous rounds, but not the rewards of unchosen actions.
        function updateWeightofSelectedArm(self,arm,reward)
            %Define the estimated reward for each arm/action to compensate the fact that the probability of chosen action i is small:
            estimatedReward = reward/self.p(arm);
            %The current weight is used to calculate next weight. %The actions that were not chosen are not updated, then those weights don't change. We only change the weight of the selected arm:
            self.w(arm)=self.w(arm)*exp(self.gamma*estimatedReward/self.n_arms) + ((exp(1)*0.0002)/self.n_arms)*sum(self.w);
        end
        
        function fixWeights(self)
           scaled =  self.w - min(self.w);
           scaled = scaled / (max(self.w)-min(self.w));
           scaled = scaled * 9 +1;
           self.w = scaled;
        end
    end
end
