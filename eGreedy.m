classdef eGreedy <handle
    
    properties
        n_arms; %number of actions, in this case: number of opponents
        epsilon; % ratio of exploration
        scores; % track expected rewards
        arm_pulls; %number of times each arm has been pulled
        cum_rewards; % accumulates rewards earned
        
        %For the regret calculation
        cumulativeReward;
    end
    
    methods
        function self = eGreedy(varargin) % initialize object
            self.n_arms = varargin{1}; % number of arms
            self.epsilon = varargin{2};
            self.scores = ones(self.n_arms,1); % initialize with 1
            self.arm_pulls = zeros(self.n_arms,1);
            self.cum_rewards = zeros(self.n_arms,1); % initialize with 0
            
            %For the regret calculation
            self.cumulativeReward = 0;
        end
        
        %Choose opponent
        function choice = chooseArm(self)       % choose an arm
            if rand <= self.epsilon             % (epsilon) times - the probability of taking random action is epsilon - 30%
                choice = randi(self.n_arms);    % explore all arms
            else                                % (1 - epsilon) times - 70%
                [~, choice] = ...               % exploit the best arm
                    max(self.scores(:,end));    % exploitation
            end
        end
        
        %Update arm data
        function updateArm(self, arm, reward)
            self.arm_pulls(arm) = self.arm_pulls(arm) + 1; % increment counter
            self.cum_rewards(arm) = self.cum_rewards(arm) + reward; % increment reward
            % compute expected reward - estimated reward
            self.scores(arm) = self.cum_rewards(arm) / self.arm_pulls(arm); % update the score of the chosen arm only for the next trial
        end
        
    end
end
