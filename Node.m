classdef Node <handle
    properties
        nodeID;
        
        %MAB
        optimal_counter;
        selection_counter_exp3;
        selection_counter_fpl;
        exp3_bandit; %node has one EXP3 brain
        egreedy_bandit; %node has one eGreedy brain
        exp3s_bandit; %node has one EXP3.S brain
        selector_trial;
        env_case;
        negotiation_result;
        
        %Arms_rewards
        rewards_arr = [0,0,0,0];
    end
    
    methods
                
        function self=Node(varargin)
            if(nargin > 0)
                self.nodeID = varargin{1};
                %MAB
                self.optimal_counter = 0;
                self.selection_counter_exp3 = 0;
                self.selection_counter_fpl = 0;
                self.selector_trial = 0;
                self.env_case = 0;
            end
        end
        
        %%%%%MAB%%%%%
        function fixNaN(self)
            bandit = self.exp3_bandit;
            bandit.fixWeights;
            
            bandit = self.exp3s_bandit;
            bandit.fixWeights;
        end
        
        function reward = executeeGreedy(self)
            bandit = self.egreedy_bandit;

            %eGreedy - MAB code
            %Draw the next opponent i_t randomly according to the expected reward
            %Decide which arm to pull based on epsilon and expected rewards
            opponent_idx = bandit.chooseArm;
            
            %eGreedy - MAB code
            %Observes reward
            reward = self.rewards_arr(opponent_idx);
            
            %eGreedy - Cumulative reward to compute regret at trial t
            bandit.cumulativeReward = bandit.cumulativeReward + reward;
            
            %eGreedy - Printing results
            reward = round(reward,2);
            
            %eGreedy - MAB code
            %Update the probability for each arm (i.e. update expected score for the arm selected in this trial)
            bandit.updateArm(opponent_idx,reward);
        end
        
        function reward = executeEXP3(self)
            bandit = self.exp3_bandit;
            
            %EXP3 - MAB code
            %At each trial/round, set p_i(t) where t is j or trial
            bandit.setProbability_i_at_t;

            %EXP3 - MAB code
            %Draw the next opponent i_t randomly according to the distribution of p_i(t)
            %Decide which arm to pull based on weights
            opponent_idx = bandit.drawOpponentAccordingProbability;
            
            %EXP3 - MAB code
            %Observes reward
            reward = self.rewards_arr(opponent_idx);
            
            %EXP3 - Cumulative reward to compute regret at trial t
            bandit.cumulativeReward = bandit.cumulativeReward + reward;
            
            %EXP3 - Printing results
            reward = round(reward,2);
            
            %EXP3 - MAB code
            %Update the probability for each arm
            bandit.updateWeightofSelectedArm(opponent_idx,reward);
        end
        
        function reward = executeEXP3S(self)
            bandit = self.exp3s_bandit;
            
            %EXP3.S - MAB code
            %At each trial/round, set p_i(t) where t is j or trial
            bandit.setProbability_i_at_t;

            %EXP3.S - MAB code
            %Draw the next opponent i_t randomly according to the distribution of p_i(t)
            %Decide which arm to pull based on weights
            opponent_idx = bandit.drawOpponentAccordingProbability;
            
            %EXP3.S - MAB code
            %Observes reward
            reward = self.rewards_arr(opponent_idx);
            
            %EXP3.S - Cumulative reward to compute regret at trial t
            bandit.cumulativeReward = bandit.cumulativeReward + reward;
            
            %EXP3.S - Printing results
            reward = round(reward,2);
            
            %EXP3.S - MAB code
            %Update the probability for each arm
            bandit.updateWeightofSelectedArm(opponent_idx,reward);
        end
        
        function getArmsRewards(self)
            
            switch(self.env_case)
                case 1
                    % first arm has higher probability to be the best arm
                    %biases = [0.8, 0.2, 0.11];
                    if rand < 0.8
                      self.rewards_arr(1) = 1;
                    else
                      self.rewards_arr(1) = 0;
                    end
                    
                    if rand < 0.2
                      self.rewards_arr(2) = 1;
                    else
                      self.rewards_arr(2) = 0;
                    end
                    
                    if rand < 0.11
                      self.rewards_arr(3) = 1;
                    else
                      self.rewards_arr(3) = 0;
                    end
                case 2
                    % last arm has higher probability to be the best arm
                    %biases = [0.11, 0.2, 0.8];
                    if rand < 0.11
                      self.rewards_arr(1) = 1;
                    else
                      self.rewards_arr(1) = 0;
                    end
                    
                    if rand < 0.2
                      self.rewards_arr(2) = 1;
                    else
                      self.rewards_arr(2) = 0;
                    end
                    
                    if rand < 0.8
                      self.rewards_arr(3) = 1;
                    else
                      self.rewards_arr(3) = 0;
                    end
                case 3
                    % second arm has higher probability to be the best arm
                    %biases = [0.11, 0.8, 0.2];
                    if rand < 0.11
                      self.rewards_arr(1) = 1;
                    else
                      self.rewards_arr(1) = 0;
                    end
                    
                    if rand < 0.8
                      self.rewards_arr(2) = 1;
                    else
                      self.rewards_arr(2) = 0;
                    end
                    
                    if rand < 0.2
                      self.rewards_arr(3) = 1;
                    else
                      self.rewards_arr(3) = 0;
                    end
            end
         
            if numel(find(self.rewards_arr)) ~= 0
                [argvalue, ~] = max(self.rewards_arr(1:3)); %if there are two equal rewards between arms, then any of those can be the optimal or argmax
            else
                argvalue = 0;
            end
            % Saving optimal arm and reward (BE WARNED: not the same as best-fixed arm)
            self.rewards_arr(4) = argvalue;
        end
    end
end