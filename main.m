%Simulation parameters
num_agents = 1;
num_opponents = 3;
epochs = 25;
num_trials = 200; %Number of trials per epoch
num_simulations = 100; %Number of simulations per exp_factor to be averaged

rng(1); %for reproducibility

simulation = Simulation(num_simulations, epochs, num_trials, num_agents, num_opponents);

exp_factor = [0 0.1 0.2 0.3 0.4 0.5 0.6 0.7 0.8 0.9 1];

file_res1 = strcat('results100_eGreedy','.csv');
fileID1 = fopen(file_res1,'w');

file_res2 = strcat('results100_EXP3','.csv');
fileID2 = fopen(file_res2,'w');

file_res3 = strcat('results100_SEXP3S','.csv');
fileID3 = fopen(file_res3,'w');

file_res4 = strcat('results100_bestfixed','.csv');
fileID4 = fopen(file_res4,'w');

file_res5 = strcat('results100_optimal','.csv');
fileID5 = fopen(file_res5,'w');

for i=1: length(exp_factor)
    solution = simulation.runSimulations(exp_factor(i));
    
    %eGreedy
    allOneString_egreedy = sprintf('%0.2f,' , solution(:,1));
    allOneString_egreedy = allOneString_egreedy(1:end-1);
    allOneString_egreedy = strcat(num2str(exp_factor(i)),',',allOneString_egreedy);
    fprintf(fileID1,'%s\n',allOneString_egreedy);
    
    %EXP3
    allOneString_exp3 = sprintf('%0.2f,' , solution(:,2));
    allOneString_exp3 = allOneString_exp3(1:end-1);
    allOneString_exp3 = strcat(num2str(exp_factor(i)),',',allOneString_exp3);
    fprintf(fileID2,'%s\n',allOneString_exp3);
    
    %EXP3.S
    allOneString_sexp3s = sprintf('%0.2f,' , solution(:,3));
    allOneString_sexp3s = allOneString_sexp3s(1:end-1);
    allOneString_sexp3s = strcat(num2str(exp_factor(i)),',',allOneString_sexp3s);
    fprintf(fileID3,'%s\n',allOneString_sexp3s);
    
    %best single action cumulative reward
    allOneString_bf = sprintf('%0.2f,' , solution(:,4));
    allOneString_bf = allOneString_bf(1:end-1);
    allOneString_bf = strcat(num2str(exp_factor(i)),',',allOneString_bf);
    fprintf(fileID4,'%s\n',allOneString_bf);
    
    %optimal action cumulative reward - optimal is the best action at every
    %trial
    allOneString_o = sprintf('%0.2f,' , solution(:,5));
    allOneString_o = allOneString_o(1:end-1);
    allOneString_o = strcat(num2str(exp_factor(i)),',',allOneString_o);
    fprintf(fileID5,'%s\n',allOneString_o);
end

fclose(fileID1);
fclose(fileID2);
fclose(fileID3);
fclose(fileID4);
fclose(fileID5);
